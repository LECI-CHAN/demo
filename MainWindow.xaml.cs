﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Штрихкоды
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private БазаШтрихкодовEntities entities = new БазаШтрихкодовEntities();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            string text = txtBarcode.Text;
            char[] array = text.ToCharArray();

            string codeCountry = "";
            for (int i = 0; i < 3; i++)
                codeCountry += array[i];

            txtCodeCountry.Text = codeCountry;

            string codeProducter = "";

            for (int i = 3; i < 7; i++)
                codeProducter += array[i];

            txtProducter.Text = codeProducter;

            string codeProduct = "";
            for (int i = 7; i < array.Length - 1; i++)
                codeProduct += array[i];

            txtCodeProduct.Text = codeProduct;

            txtCountry.Text = entities.ПоискСтраны(codeCountry).Single().ToString();

            txtNameProduct.Text = entities.ПоискТовара(codeProducter, Convert.ToInt64(codeProduct)).Single().Товар;
        }
    }
}
